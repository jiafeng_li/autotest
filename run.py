import pytest
from Common import Shell
from Common import Email
from Common import Log
from Conf import Config

if __name__ == '__main__':
    shell = Shell.Shell()
    log = Log.MyLog()
    config = Config.Config()

    #清除报告文件
    clean_cmd = 'rm -rf ./Report/*'
    shell.invoke(clean_cmd)

    #清除log日志
    clean_err_log = 'echo ''> ./Log/err.log'
    shell.invoke(clean_err_log)
    clean_info_log = 'echo ''> ./Log/log.log'
    shell.invoke(clean_info_log)

    # 定义测试集
    result_path = config.result_path
    report_path = config.report_path
    args = ['TestCase', '--alluredir', result_path]
    pytest.main(args)

    #生成报告
    cmd = 'allure generate %s -o %s' % (result_path, report_path)
    try:
        shell.invoke(cmd)
    except Exception:
        print("执行用例失败")
        raise

    #发送邮件
    try:
        mail = Email.SendMail()
        mail.sendMail()
    except Exception as e:
        log.error('发送邮件失败')
        raise

