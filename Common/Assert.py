# -*- coding: utf-8 -*-
# @Time    : 2021/8/28 10:00
# @Author  : jiafeng.li
# @File    : Assert.py

'''
封装Assert方法
'''

from Common import Log
from Common import Consts
import json

class Assert:
    def __init__(self):
        self.log = Log.MyLog()

    def asser_code(self, code, expect_code):
        '''
        验证状态码
        :param code:
        :param expect_code:
        :return:
        '''
        try:
            assert code == expect_code
            return True
        except:
            self.log.error("StatusCode error expect_code is %s StatusCode is %s" % (expect_code, code))
            Consts.RESULT_LIST.append('fail')
            raise

    def asser_body(self, body, body_msg, expect_msg):
        '''
        验证respond body中的任意属性
        :param body:
        :param body_msg:
        :param expect_msg:
        :return:
        '''
        try:
            msg = body[body_msg]
            assert msg == expect_msg
            return True
        except:
            self.log.error("Respond body msg != expect_msg, expect_msg is %s , body_msg is %s" % (expect_msg, body_msg))
            Consts.RESULT_LIST.append('fail')
            raise

    def asser_in_text(self, body, expect_msg):
        '''
        验证respond body中是否存在期望的字符串
        :param body:
        :param expect_msg:
        :return:
        '''
        try:
            text = json.dumps(body, ensure_ascii=False)
            #print(text)
            assert expect_msg in text
            return True
        except:
            self.log.error("Response body Does not contain expected_msg, expect_msg is %s" % expect_msg)
            Consts.RESULT_LIST.append('fail')
            raise

    def asser_text(self, body, expect_msg):
        '''
        验证respond body是否等于预期的字符串
        :param body:
        :param expect_msg:
        :return:
        '''
        try:
            assert body == expect_msg
            return True
        except:
            self.log.error("Response body != expected_msg, expect_msg is %s , body is %s" % (expect_msg, body))
            Consts.RESULT_LIST.append('fail')
            raise

    def asser_time(self, time, expect_time):
        '''
        验证respond body响应时间是否小于预期时间
        :param time:
        :param expect_time:
        :return:
        '''
        try:
            assert time < expect_time
            return True
        except:
            self.log.error("Response time > expect_time, expect_time is %s, time is %s" % (expect_time, time))
            Consts.RESULT_LIST.append('fail')
            raise