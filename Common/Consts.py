# -*- coding: utf-8 -*-
# @Time    : 2021/8/24 10:09
# @Author  : jiafeng.li
# @File    : Consts.py

'''
接口全局变量
'''

# 接口全局配置
API_ENVIRONMENT_DEBUG = 'debug'
API_ENVIRONMENT_RELEASE = 'release'
API_SHIPMENT = 'shipment'
API_ORDER = 'order'

# 接口响应时间list，单位毫秒
STRESS_LIST = []

# 接口执行结果list
RESULT_LIST = []

#接口字段定义
ORDER_ID = 'orderid'
ORDER_SN = 'ordersn'
TN = 'tracking_number'
ARRANGE_SHIPMENT_STATUS = 'package_arrange_shipment_status'
PACKAGE_NUMBER = 'package_number'
NORMAL_WAYBILL = 'can_print_normal_waybill'
JOB_ID = 'job_id'
