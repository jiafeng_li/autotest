# -*- coding: utf-8 -*-
# @Time    : 2021/8/24 10:09
# @Author  : jiafeng.li
# @File    : api.py

from Common import Consts
from Common import Request
from Params import params
from Common import Assert
from Common import Log
import jsonpath
import json
log = Log.MyLog()

class Api:
    def __init__(self):
        pass

    def Create_Order(self, region, channel):
        '''
        创建订单
        :param region:
        :param channel:
        :return:
        '''
        url = params.Basic().Get_Url('create_order')
        header = params.HeaderInfo().Get_Header_Info('create_order')

        #获取创建订单信息并插入渠道信息
        orderinfo = params.OrderInfo().Get_OrderInfo(region)

        orderinfo.update({'logistics_id': channel})

        #执行创建订单
        create_order = Request.Request().Post_Result(url, orderinfo, header)
        Assert.Assert().asser_code(create_order['code'], 200)  #断言执行订单结果
        return create_order

    def Can_Shipment_Judge(self, order_info, region_id):
        '''
        判断是否有发货按钮
        :param order_info:
        :return:
        '''
        #获取url&hearder
        can_shipment_url = params.Basic().Get_Url('basic_url') + params.ApiInfo(Consts.API_SHIPMENT, 'CAN_ORDER_ARRANGE_SHIPMENT_MULTI_SHOP').Get_Api_Info()
        can_shipment_header = params.HeaderInfo().Get_Header_Info(region_id)
        #拼接请求
        orderid = jsonpath.jsonpath(order_info, '$..' + Consts.ORDER_ID)
        orders = {}
        orders_list = []
        data = {}

        orders['order_id'] = orderid[0]
        orders['region_id'] = can_shipment_header['region-id']
        orders['shop_id'] = int(can_shipment_header['shop-id'])
        orders_list.append(orders)
        data["orders"] = orders_list
        json.dumps(data)

        #发送请求
        response = Request.Request().Post_Result(can_shipment_url, data, can_shipment_header)
        response = jsonpath.jsonpath(response, '$..list')
        if len(response[0]) == 0:
            return None
        else:
            result = jsonpath.jsonpath(response[0][0], '$..' + Consts.ARRANGE_SHIPMENT_STATUS)
            return result[0]

    def Arrange_Shipment_01(self, order_info, region_id):
        '''
        调用1.0接口发货
        :param order_info:
        :return:
        '''
        #获取url&hearder
        shipment_url = params.Basic().Get_Url('basic_url') + params.ApiInfo(Consts.API_SHIPMENT, 'ARRANGE_SHIPMENT').Get_Api_Info()
        shipment_header = params.HeaderInfo().Get_Header_Info(region_id)

        #拼接请求
        ordersn = jsonpath.jsonpath(order_info, '$..' + Consts.ORDER_SN)
        data = {}
        data['shopid'] = int(shipment_header['shop-id'])
        data['ordersn'] = ordersn[0]
        data['partner_id'] = 1
        data['dropoff'] = {}
        json.dumps(data)

        #发送请求
        response = Request.Request().Post_Result(shipment_url, data, shipment_header)
        response_tn = jsonpath.jsonpath(response, '$..' + Consts.TN)
        print(response_tn)
        if len(response_tn) == 0:
            return None
        else:
            return response_tn[0]

    def Get_Order_Detail(self, order_info, region_id):
        '''
        调用1.0接口获取订单的信息
        :param order_info:
        :return:
        '''
        #获取url&hearder
        order_detail_url = params.Basic().Get_Url('basic_url') + params.ApiInfo(Consts.API_ORDER, 'GET_PACKAGE').Get_Api_Info()
        order_detail_header = params.HeaderInfo().Get_Header_Info(region_id)

        #拼接请求
        orderid = jsonpath.jsonpath(order_info, '$..' + Consts.ORDER_ID)
        data = 'order_id=' + str(orderid[0])

        #发送请求
        response = Request.Request().Get_Request(order_detail_url, data, order_detail_header)
        return response

    def Check_Print(self, order_info,region_id):
        '''
        校验订单是否可以打印
        :param order_info:
        :return:
        '''
        #获取url&hearder
        check_url = params.Basic().Get_Url('basic_url') + params.ApiInfo(Consts.API_SHIPMENT, 'CHECK_PRINT').Get_Api_Info()
        check_header = params.HeaderInfo().Get_Header_Info(region_id)

        #拼接请求
        data = {"order_info_list": [{"order_id":jsonpath.jsonpath(order_info, '$..' + Consts.ORDER_ID)[0],
                                  "region_id": region_id,"shop_id": int(check_header['shop-id'])}]}
        json.dumps(data)

        #发送请求
        response = Request.Request().Post_Result(check_url, data, check_header)
        return response

    def Create_AWB_Job_SC(self, order_info, region_id, channel_id, file_type, file_name, file_contents):
        '''
        调用create接口创建面单任务
        :param order_info:
        :param region_id:
        :param channel_id:
        :return:
        '''
        #获取url&hearder
        create_url = params.Basic().Get_Url('basic_url') + params.ApiInfo(Consts.API_SHIPMENT, 'CREATE_SD_JOB').Get_Api_Info()
        create_header = params.HeaderInfo().Get_Header_Info(region_id)

        #获取package信息
        package_info = self.Get_Order_Detail(order_info, region_id)

        #拼接请求
        package_list = [{"channel_id": channel_id, "region_id": region_id,
                        "package_number": jsonpath.jsonpath(package_info, '$..' + Consts.PACKAGE_NUMBER)[0],
                        "order_id": jsonpath.jsonpath(package_info, '$..order_id')[0],
                         "shop_id": int(create_header['shop-id'])}]
        generate_file_details = [{"file_type": file_type, "file_name" : file_name, "file_contents": [file_contents]}]
        data = {"package_list": package_list, "generate_file_details": generate_file_details}
        json.dumps(data)

        #发送请求
        response = Request.Request().Post_Result(create_url, data, create_header)
        return response

    def Download_AWB(self, job_id, region_id):
        '''
        下载面单
        :param job_id:
        :param region_id:
        :return:
        '''
        # 获取url&hearder
        download_url = params.Basic().Get_Url('basic_url') + params.ApiInfo(Consts.API_SHIPMENT,'DOWNLOAD_SD').Get_Api_Info()
        download_header = params.HeaderInfo().Get_Header_Info(region_id)
        #请求参数
        data = 'job_id=' + job_id

        #发送请求
        response = Request.Request().Get_Request(download_url, data, download_header, True)
        return response

if __name__ == '__main__':
    a = Api()
    a.Download_AWB('SG_321239_00629915a46ffef52a4c40d435481061_g', 'SG')
    #create_order = a.Create_Order('SG', 10012)
    #a.Check_Print(1, 'SG')

