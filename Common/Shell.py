# -*- coding: utf-8 -*-
# @Time    : 2021/8/23 13:35
# @Author  : jiafeng.li
# @File    : shell.py

'''
封装执行shell语句
'''

import subprocess

class Shell:
    @staticmethod
    def invoke(cmd):
        output, errors = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
        o = output.decode("utf-8")
        return o
