# -*- coding: utf-8 -*-
# @Time    : 2021/9/04 11:13
# @Author  : jiafeng.li
# @File    : params.py

'''
定义所有测试数据
'''

import os
from Params import read_yml
from Common import Log
from Common import Consts
log = Log.MyLog()
path_dir = str(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))

def Get_Parameter(name):
    data = read_yml.GetPages().get_page_list()
    param = data[name]
    return param

'''
读取基础url配置
'''
class Basic:
    def __init__(self):
        self.url = []
        self.params = Get_Parameter('Basic')

    def Get_Url(self, name):
        log.info('解析yaml, Path:' + path_dir + '/Params/Param/Basic.yml')
        for i in range(0,len(self.params)):
            self.url.append(self.params[i])

        for url in self.url:
            for key, value in url.items():
                if key == name:
                    return value[0]['url']

'''
读取创建订单所需的信息
'''
class OrderInfo:
    def __init__(self):
        self.params = Get_Parameter('OrderInfo')
        self.data = {}

    def Get_OrderInfo(self, region):
        log.info('解析yaml, Path:' + path_dir + '/Params/Param/OrderInfo.yml')
        for param in range(0,len(self.params)):
            for key, value in self.params[param].items():
                if key == region:
                    self.data = value[0]
        return self.data

'''
读取API信息
'''
class ApiInfo:
    def __init__(self, model_name, api_name):
        self.model_name = model_name
        self.api_name = api_name
        self.params = Get_Parameter('ApiInfo')
        self.model = []

    def Get_Api_Info(self):
        log.info('解析yaml, Path:' + path_dir + '/Params/Param/ApiInfo.yml')
        for param in range(0, len(self.params)):
            for key, value in self.params[param].items():
                if key == self.model_name:
                    try:
                        self.model.append(self.model_name)
                        return value[0][self.api_name]
                    except Exception as e:
                        log.info('the api does not exit')
                        print('the api does not exit')
                        return None
        if len(self.model) == 0:
            log.info('the model does not exit')
            print('the model does not exit')
            return None

'''
读取header信息
'''
class HeaderInfo:
    def __init__(self):
        self.params = Get_Parameter('HeaderInfo')
        self.data = {}

    def Get_Header_Info(self, name):
        log.info('解析yaml, Path:' + path_dir + '/Params/Param/HeaderInfo.yml')
        for param in range(0,len(self.params)):
            for key, value in self.params[param].items():
                if key == name:
                    self.data = value[0]
        return self.data



if __name__ == '__main__':
    param = HeaderInfo().Get_Header_Info('create_order')
    print(param)




