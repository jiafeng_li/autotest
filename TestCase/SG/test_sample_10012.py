# -*- coding: utf-8 -*-
# @Time    : 2021/8/24 10:09
# @Author  : jiafeng.li
# @File    : test_sample_10012.py

import allure
from Common import Api
import jsonpath
from Common import Consts
import time
import os

class Test_Sample_10012:
    @allure.feature('10012渠道打印面单')
    @allure.severity('normal')
    @allure.story('创建订单')
    def test_create_order_10012(self):
        #创建订单
        global create_order
        create_order = Api.Api().Create_Order('SG', 10012)

        #判断订单是否有发货按钮
        while True:
            if Api.Api().Can_Shipment_Judge(create_order, 'SG') == 1:
                print('can arrange shipment')
                break
            else:
                print('can not arrange shipment')
    #endef

    @allure.feature('10012渠道打印面单')
    @allure.severity('normal')
    @allure.story('发货')
    def test_arrange_shipment_10012(self):
        flag = 0
        global  arrange_result, ordertn
        arrange_result = 0
        #发货 & 获取TN
        while True:
            ordertn = Api.Api().Arrange_Shipment_01(create_order, 'SG')
            if ordertn != None:
                print(ordertn)
                print('arrange successfully')
                break
            else:
                flag += 1
                print('arrange failed')

            #连续10次无法获取到TN，中断获取
            if flag == 10:
                arrange_result = 1
                break
    #endef

    @allure.feature('10012渠道打印面单')
    @allure.severity('normal')
    @allure.story('打印面单')
    def test_print_AWB(self):
        #未发货成功直接终止
        if arrange_result == 1:
            return ()
        check = 0

        #校验是否可以打印
        while True:
            check_print = Api.Api().Check_Print(create_order, 'SG')
            if jsonpath.jsonpath(check_print, '$..' + Consts.NORMAL_WAYBILL)[0] == True:
                break
            else:
                check += 1
            if check == 50:
                break
        if check == 50:
            print('check failed')
            return ()

        #创建面单任务
        create_response = Api.Api().Create_AWB_Job_SC(create_order, 'SG', 10012, "THERMAL_PDF", "Air Waybill", 3)
        print(create_response)
        job_id = jsonpath.jsonpath(create_response, '$..' + Consts.JOB_ID)[0]

        #打印面单
        time.sleep(5)
        download_result = Api.Api().Download_AWB(job_id, 'SG')
        pdf_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'SG_10012_Normal.pdf')
        with open(pdf_path, 'wb') as f:
            f.write(download_result.content)










